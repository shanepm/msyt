use crate::{
  Result,
  model::Content,
};
use byteordered::Endian;
use failure::ResultExt;
use msbt::Header;

pub fn parse_controls(header: &Header, s: &[u8]) -> Result<Vec<Content>> {
  let mut parts = Vec::new();

  let bytes: Vec<u16> = s
    .chunks(2)
    .map(|x| header.endianness().read_u16(x)
      .with_context(|_| "could not read bytes")
      .map_err(Into::into))
    .collect::<Result<_>>()?;
  let from = if bytes[bytes.len() - 1] == 0 {
    &bytes[..bytes.len() - 1]
  } else {
    &bytes
  };
  let string = String::from_utf16(&from).with_context(|_| "could not parse utf-16 string")?;
  if !string.is_empty() {
    parts.push(Content::Text(string));
  }

  Ok(parts)
}
